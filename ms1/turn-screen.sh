#===============================================================================
#USAGE : ./turnscreen.sh orientation
#DESCRIPTION : Pass as an argument de degrees of the rotation of the screen.
#PARAMS: orientation can be right, left, inverted
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
echo "Please type below the direction in which you want to rotate the screen (left / right)"

read i

if [ $i == "left" ] 
    then
	    echo "Rotate to left"
	    xrandr -o left
	
elif [ $i =="right" ] 
    then
	    echo "Rotate to right"
	    xrandr -o right
	
else 
	echo "invalid operation, please try again"
fi

#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
