#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS: 
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
printf "\n (1) Shutdown \n (2) Reboot \n (3) Rotate Screen \n (4) Change keyboard language \n Select an action : "

read i

if [ $i == 1 ] 
then
echo "Are you sure that you want to shutdown the computer ? (y / n)"

read i

if [ $i = y ];
    then
        shutdown -h now
elif [ $i = n ];
    then
        echo "cancelling shutdown"
else
    echo "invalid operation, please try again"
fi

elif [ $i == 2 ]
then
echo "The system will restart in 5 seconds"
for i in {5..1}
do
 echo $i
 sleep 1
done
reboot

elif [ $i == 3 ]
then 
echo "Please type below the direction in which you want to rotate the screen (left / right)"

read i

if [ $i == "left" ] 
    then
	    echo "Rotate to left"
	    xrandr -o left
	
elif [ $i =="right" ] 
    then
	    echo "Rotate to right"
	    xrandr -o right
	
else 
	echo "invalid operation, please try again"
fi

elif [ $i == 4 ]
then
echo "Type your prefered language code below"
read i
setxkbmap $i
echo "Changing your keyboard language to" $i
fi
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
